import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Welcome to my Pretty App',
        theme: ThemeData(
          primarySwatch: Colors.teal,
          fontFamily: 'Georgia',
          brightness: Brightness.dark,
        ),
        home: AnimatedSplashScreen(
          splash: 'assets/image-placeholder.png', // use any widget here
          nextScreen: MyHomePage(),
          splashTransition: SplashTransition.rotationTransition,
          duration: 2000,
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 150,
              height: 150,
              color: Theme.of(context).colorScheme.primary,
              child: Image.asset('assets/image-placeholder.png'),
            ),
            Container(
              child: Text(
                "Welcome to my Pretty App",
                style: TextStyle(fontSize: 40),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
